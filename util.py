import os

class Util:

    # delete file
    @staticmethod
    def deleteFile(file):
        if os.path.isfile(file):
            os.remove(file)

    # clear
    @staticmethod
    def clearFolder(directory):
        if os.path.exists(directory):
            for dirPath, dirNames, files in os.walk(directory):
                for file in files:
                    filePath = os.path.join(dirPath, file)
                    #fd = os.open( file, os.O_RDWR|os.O_CREAT )
                    #os.close( fd )

                    os.remove(filePath)

                for dirName in dirNames:
                    Util.clearFolder( os.path.join(dirPath, dirName) )

            os.rmdir(directory)

    # createFolder
    # verifie l'existance du dossier test si non le crée
    @staticmethod
    def createFolder(folder):
        if not os.path.exists(folder):
            os.makedirs(folder)

    # todo make comment
    @staticmethod
    def clearAndCreateFolder(folder):
        Util.clearFolder(folder)
        Util.createFolder(folder)

    @staticmethod
    def get_all_points(clusters, lvl=0):
        points = []
        for index, cluster in enumerate(clusters):
            points.append( cluster["point"] )

            points_tmp = Util.get_all_points(cluster["clusters"], lvl+1)
            
            if len(points_tmp) > 0:
                points.extend( points_tmp )
        
        return points

    @staticmethod
    def get_all_clusters_to_string(clusters):
        values = []
        for index, cluster in enumerate(clusters):
            dic = Util.get_cluster_as_dic(cluster)

            values.append(dic)

        return values

    @staticmethod
    def get_cluster_as_dic(cluster):
        return {
            "cluster" :
                {
                    "id" : cluster["id"],
                    "point" : {
                        "x" : str(cluster["point"][0]), # Json do not support float type
                        "y" : str(cluster["point"][1])
                    },
                    "clusters" : Util.get_all_clusters_to_string( cluster["clusters"] ),
                    "files" : cluster["files"]
                }
        }