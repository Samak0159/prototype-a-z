from sklearn import manifold # https://github.com/jiangqy/t-SNE/blob/master/t-SNE.ipynb
import numpy as np

class Tsne:
    def __init__(self, X):
        self.X = X


    def t_SNE(self):
        print("Start T_SNE")

        tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
        X_tsne = tsne.fit_transform( self.X )

        x_min, x_max = np.min(X_tsne, 0), np.max(X_tsne, 0)
        X = (X_tsne - x_min) / (x_max - x_min)

        return X