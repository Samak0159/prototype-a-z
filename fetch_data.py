import os, random
import numpy as np

from data_extractor import DataExtractor
from image_util import ImageUtil
from Tsne import Tsne

class FetchData:
    # todo supprimer les fonctions non utilisé

    
    def __init__(self, file_name, data_dtype=np.float32):
        self.file_name = file_name
        self._data_dtype = data_dtype
    
    def initialize(self):
        if self.file_name.find(".txt") > -1:
            self._readDataFromTxtFile()
        elif self.file_name.find(".") == -1:
            file_path = self.file_name + "_tsne_data.dat"

            if not os.path.isfile(file_path):
                folder = "images_scharr"
                files = ImageUtil.getAllFiles(folder)

                data_images, data_labels, data_paths = DataExtractor.getDataScharrAndLabels( self.file_name, files, np.int16 , doThreshold=True)

                # print("data_images : ", data_images.shape)
                # print("data_labels : ", data_labels.shape)
                # print("data_paths  : ", data_paths.shape)

                # filtre pour la soulager la RAM
                labels_unique = np.unique(data_labels)
                clusters_n = labels_unique.size
                # print("clusters_n : ", clusters_n)
                number_element = 50 # todo magic number todo utiliser config

                # todo les dtypes doivent etre ecrit dans config
                data = np.array([], dtype=self._data_dtype)
                labels = np.array([], dtype=np.int16) # todo np.int16 ne doit pas etre ecrit en dure
                paths = np.array([], dtype=np.str) # todo np.str ne doit pas etre ecrit en dure

                for i,label in enumerate(labels_unique):
                    print("## cluster : ", i, "/", (clusters_n-1))
                    idx = np.where( data_labels == label )[0]

                    if idx.shape[0] < number_element:
                        for number in range(idx.shape[0]):
                            index = idx[number]
                            
                            data = np.append( data, data_images[index] )
                            labels = np.append( labels, data_labels[index] )
                            paths = np.append( paths, data_paths[index] )
                    else:
                        for _ in range( number_element ):
                            random_number = random.randint(0, idx.shape[0]-1)
                            index = idx[random_number]
                            
                            idx = np.delete(idx, random_number)
                            
                            data = np.append( data, data_images[ index ] )
                            labels = np.append( labels, data_labels[ index ] )
                            paths = np.append( paths, data_paths[index] )

                 # Clear Memory
                del folder
                del files
                del data_images
                del data_labels
                del data_paths

                # reshape
                data = data.reshape( labels.shape[0], int(data.shape[0] / labels.shape[0]) )

                # Tsne
                data_tsne = Tsne( data ).t_SNE()

                # write Data
                DataExtractor.writeMemMap(file_path, data_tsne, self._data_dtype)
                # override Data
                # todo "_tsne_labels.dat" --> magic strinc --> todo utiliser config
                save_labels_file = self.file_name + "_tsne_labels.dat"
                ImageUtil.deleteFile( save_labels_file ) # delete pour override # todo utiliser util
                DataExtractor.writeMemMap(save_labels_file, labels, np.int16) # todo np.int16 ne doit pas etre ecrit en dure --> todo utiliser config
                # Paths
                # todo "_paths.csv" --> magic strinc --> todo utiliser config
                save_path_file = self.file_name + "_paths.csv"
                DataExtractor.writeCSVFile(save_path_file, paths)

            # todo les dtypes doivent etre ecrit dans config
            data = DataExtractor.readMemmap( file_path , self._data_dtype)
            labels = DataExtractor.readMemmap( self.file_name + "_tsne_labels.dat", np.int16 ) # np.int16 en dure
            paths = DataExtractor.readCSVFile( self.file_name + "_paths.csv", np.str ) # np.str en dure

            # todo Certaine variable ne sont pas utilisé, supprimer les trucs inutile
            self._data = data.reshape( labels.shape[0], int(data.shape[0] / labels.shape[0]) )
            self._names = labels # ATTENTION LABELS CORRESPOND A L'INDEX DE names[] --> voir dataExtractor
            self._labels = np.arange( self._names.shape[0] )
            self._files = paths.reshape( -1 )
            
        else:
            assert ValueError("FeatchData initialize() don't know how to extract data from %s " % self.file_name)

        return self
    
    def _readDataFromTxtFile(self):
        # Read Data
        fo = open(self.file_name, "r")
        data_read = fo.read()
        fo.close()

        # print("data_read : ", data_read)

        array = data_read.split("\n")

        self._data = np.array([], dtype=self._data_dtype)
        self._names = np.array([], dtype=str)
        self._labels = np.array([], dtype=np.int16)

        for index, element in enumerate(array):
            if element.find("|") == -1:
                continue
            item = element.split("|")

            # Name / Label
            name = item[0]
            self._names = np.append(self._names, name)
            
            self._labels = np.append(self._labels, index)

            # data
            values_array_str = item[1].strip(" ").split(" ")
            self._data = np.append(self._data, values_array_str).astype(self._data_dtype) # extends data

        # clean
        del values_array_str
        del name

        self._files = sorted( ImageUtil.getAllFiles( "images" ) )
        
        # Shape
        self._data = self._data.reshape( self._names.shape[0], int(len(self._data)/self._names.shape[0]) )

    def getNames(self):
        return self._names
    
    def getLabels(self):
        return self._labels
    
    def getData(self):
        return self._data

    def getFiles(self):
        return self._files

    def getValues(self):
        return self._data, self._labels, self._names, self._files

    def get_sample(self, data_images, data_labels, data_paths):
        # print("data_images : ", data_images.shape)
        # print("data_labels: ", data_labels.shape)
        # print("data_paths : ", data_paths.shape)

        labels_unique = np.unique(data_labels)
        clusters_n = labels_unique.size
        # print("clusters_n : ", clusters_n)
        number_element = 50 # todo magic number

        data = np.array([], dtype=self._data_dtype)
        labels = np.array([], dtype=np.int16) # todo np.int16 ne doit pas etre ecrit en dure
        paths = np.array([], dtype=np.str) # todo np.str ne doit pas etre ecrit en dure

        # print("labels_unique : ", labels_unique)
        # exit()
        cluster_to_skip = []
        cluster_to_skip.append(0)
        cluster_to_skip.append(1)
        # 2

        for i,label in enumerate(labels_unique):
            print("## cluster : ", i, "/", (clusters_n-1))

            if i == 4:
                break;

            if i in cluster_to_skip:
                continue

            # print("i : ", i)
            # print("label : ", label)

            idx = np.where( data_labels == label )[0]

            if idx.shape[0] < number_element:
                for number in range(idx.shape[0]):
                    index = idx[number]
                    
                    data = np.append( data, data_images[index] )
                    labels = np.append( labels, data_labels[index] )
                    paths = np.append( paths, data_paths[index] )
            else:
                for _ in range( number_element ):
                    random_number = random.randint(0, idx.shape[0]-1)
                    index = idx[random_number]
                    
                    idx = np.delete(idx, random_number)
                    
                    data = np.append( data, data_images[ index ] )
                    labels = np.append( labels, data_labels[ index ] )
                    paths = np.append( paths, data_paths[index] )

            # print("label : ", label)
        # exit()
        # reshape
        data = data.reshape( labels.shape[0], int(data.shape[0] / labels.shape[0]) )

        # print("data : ", data.shape)
        # print("labels: ", labels.shape)
        # print("paths : ", paths.shape)
        # from Graph import Graph
        
        # New version
        # g = Graph(save_override=True).initialize()
        # id = 0
        # for element in data:
        #     point = [float(element[0]), float(element[1])]

        #     id += 1
        #     g.add_point( point, id, 0)
        # g.save_graph()
        # g.show_graph()
        # print("done")
        


        return data, labels, paths
