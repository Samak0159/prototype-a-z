import json

config_file = "config.json"

class Config:
    _instance = None
    def __init__(self):
        # lit le fichier
        with open(config_file, 'r') as f:
            config = json.load(f)
            
        # Image
        self.image = type('Image', (), {})
        self.image = self.image()
        self.image.height    = config["Images"]["Property"]["height"]
        self.image.width     = config["Images"]["Property"]["width"]
        self.image.channels  = config["Images"]["Property"]["channels"]
        self.image.colored   = config["Images"]["Property"]["colored"]
        self.image.doBlur    = config["Images"]["Property"]["doBlur"]
        self.image.extension = config["Images"]["Property"]["extension"]

        # Image Path Config
        self.image.path = type('Path', (), {})
        self.image.path.input        = config["Images"]["Paths"]["input"]
        self.image.path.tmp          = config["Images"]["Paths"]["tmp"]
        self.image.path.output       = config["Images"]["Paths"]["output"]
        self.image.path.dataset      = config["Images"]["Paths"]["dataset"]
        self.image.path.datasetTrain = config["Images"]["Paths"]["datasetTrain"]
        self.image.path.datasetTest  = config["Images"]["Paths"]["datasetTest"]
        self.image.path.predict      = config["Images"]["Paths"]["predict"]

        # Face Detection
        self.face_detection = type('FaceDetection', (), {})
        self.face_detection.folder = config["DetectFaces"]["cascadeFolder"]
        self.face_detection.file = config["DetectFaces"]["cascadeFile"]
