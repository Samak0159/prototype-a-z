import requests # call api
import urllib.request # download videos
import urllib.error 
from unidecode import unidecode

import os, json, time
from image_util import ImageUtil


class DownloadVideo:
    # todo mettre des logs en place
    # Doc de l'api --> Google Drive --> PUBLIC API DOC
    # Pour avoir le password (apiSecret) --> Intellij plugin speachme --> Send Api key 

    def __init__(self, login, password, hosts_url=None):
        # Initialisation
        self.hosts_url = hosts_url if hosts_url is not None else  "https://inside.speach.me"
        self.login = login
        self.password = password
        self.jsons_folder = "speachs_jsons" # todo mettre dans config
        self.videos_folder = "videos" # todo mettre dans config
        self.api_langue = "fr"
        self.api_version = "1"

        self._initialisation()

    def _initialisation(self):
        print("DownloadVideo")
        # Crée les dossiers
        # todo remplacer ImageUtil par util
        ImageUtil.createFolder( self.jsons_folder )
        ImageUtil.createFolder( self.videos_folder )

        # Authenticate
        self._authentificate()
        
        # Search --> Recupere les speachs
        self._search()

        # Video Url --> Recupere les urls des videos des speachs
        self._get_video_url()

        # Download
        self._download_video()
    
    def _authentificate(self):
        print("Authenticate")
        authenticate_suffix_url = "/api/authenticate"
        authenticate_url = self.hosts_url + authenticate_suffix_url

        params = {}
        params["apiKey"] = self.login
        params["apiSecret"] = self.password
        params["lang"] = self.api_langue
        params["version"] = self.api_version

        authenticate_url = self._generateUrl(authenticate_url, params)

        # requete
        # verify=False : Authorise les requetes sur HTTPS sans certif valide
        authentificate_response = requests.get(authenticate_url, verify=False)
        authentificate_array = authentificate_response.json()

        self.sessionId = authentificate_array["data"]

    def _search(self):
        print("Search")
        # Search 
        search_url_suffix = self.hosts_url + "/api/speach/search"

        params = {}
        params["sessionId"] = self.sessionId
        params["lang"] = self.api_langue
        params["version"] = self.api_version
        params["objectByPage"] = "100"
        params["page"] = "1"

        search_url = self._generateUrl(search_url_suffix, params)
        #print("search_url : ", search_url)

        # verify=False : Authorise les requetes sur HTTPS sans certif valide
        search_response = requests.get(search_url, verify=False)
        search_array = search_response.json()


        # initialisation
        data = search_array["data"]

        # Get Json
        index = 1
        while( len(data) > 0 ):
            
            for speach in data:
                # title
                print("title : ", speach["title"])
                title = unidecode( speach["title"]
                                    .replace(" ","_")
                                    .replace("/","")
                                    .replace("\\","")
                                    .replace("[","")
                                    .replace("]","")
                                    .replace("\"","")
                                    .replace("-","")
                                    .replace(",","")
                                    .replace("!","")
                                    .replace("?","")
                                    .replace(".","")
                                    )
                #print("## speach : ")
                #print(speach)

                # write json file
                # todo utiliser une fonction global d'ecriture
                filename = os.path.join( self.jsons_folder, title + ".json")
                with open(filename, 'w') as file:
                    file.write(json.dumps(speach))

            # New Requete
            index += 1
            params["page"] = str(index)

            search_url = self._generateUrl(search_url_suffix, params)
            # verify=False : Authorise les requetes sur HTTPS sans certif valide
            search_response = requests.get(search_url, verify=False)
            search_array = search_response.json()

            data = search_array["data"]

    def _get_video_url(self):
        print("GetVideoUrl")
        # todo effectué le test :
        # actuellement nous prenons les videos (haut qualité) via URL (dans la base)
        # on peut tester ce que ça donner avec les videos basse qualité URLLQ (dans la base)

        # Lecture des jsons
        jsons = ImageUtil.getAllFiles( self.jsons_folder )

        self.urls_dic = []
        for json_file in jsons:
            json_data = open(json_file).read()
            speach = json.loads(json_data)

            # User
            user = speach["user"]

            userName = user["firstName"] + "_" + user["lastName"]
        
            # Lesson
            for lesson in speach["lessons"]:
                # Chapter
                for chapter in lesson["chapters"]:
                    #print("###")
                    #print("json_file : ", json_file)
                    #print("chapter id : ", chapter["id"])

                    # Contenu média
                    if "contentMedia" in chapter:
                        contentMedia = chapter["contentMedia"]
                        
                        contentType = contentMedia["type"]
                        #print("type : ", contentType)
                        # Pour le POKE on ne prend que les videos webcam, mais url ici est en préparation de l'avenir
                        if contentType == "VIDEO" or contentType == "VIDEO_UPLOAD":
                            mediaInfo = contentMedia["mediaInfo"]

                            url = mediaInfo["url"]
                        elif contentType == "WEBCAM" or contentType == "SCREEN_WEBCAM" or contentType == "SLIDE_DECK_WC_PPT" or contentType == "WC_PPT":
                            url = contentMedia["url"]

                            self.urls_dic.append({
                                "user": userName,
                                "url": url
                            })
                        elif contentType == "SCREEN_AUDIO" or contentType == "AUDIO_PPT" or contentType == "SLIDE_DECK_AUDIO_PPT" or contentType == "SCREEN" or contentType == "VIDEO_UPLOAD_PPT" or contentType == "SLIDE_DECK_VIDEO_UPLOAD_PPT" or contentType == "AUDIO_UPLOAD_PPT" or contentType == "AUDIO" or contentType == "VIDEO_PPT":
                            continue
                        else:
                            raise ValueError("contentType non traiter : ", contentType)
                    else:
                        #print("pas de contentMedia : ", chapter["id"])
                        pass
        
    def _generateUrl(self, url, params):
        index = 0
        for key in params:
            if index == 0:
                concatenation_char = "?"
            else :
                concatenation_char = "&"
            
            url += concatenation_char + key + "=" + params[key]
            
            index += 1
        
        return url

    def _getVideoName(self, user, compteur=0):
        video_name = user + "_" + str(compteur) + ".mp4"
        video_enr = os.path.join( self.videos_folder, video_name )

        if os.path.isfile(video_enr):
            return self._getVideoName(user, (compteur + 1) )
        else:
            return video_name

    def _download_video(self):
        print("_downLoadVideo")
        print("len(urls_dic) : ", len(self.urls_dic) )

        for dic in self.urls_dic:    
            video_name = self._getVideoName(dic["user"])
            
            try:
                video_enr = os.path.join( self.videos_folder, video_name )
                urllib.request.urlretrieve(dic["url"], video_enr)

                time.sleep(1)
            except urllib.error.HTTPError:
                print("HTTP error on url : ", dic["url"])