import math
import numpy as np

from graph import Graph
from util import Util

class Clusters:

    def __init__(self, data, files):
        self.clusters = []
        self.graph = Graph(save_override=False).initialize()
        self.enough_cluster = False

        for i, point in enumerate(data):
            id = self._new_id()

            files_path = []
            files_path.append( files[i] )

            cluster = self._make_cluster(id, point, [], files_path)
            self.clusters.append(cluster)

            self.graph.add_point(point, id, 0)

    def _new_id(self):
        max_id = 0
        for cluster in self.clusters:
            id = cluster["id"]

            if id > max_id:
                max_id = id
        return max_id+1

    def _get_index(self, id):
        for index, cluster in enumerate(self.clusters):
            if cluster["id"] == id:
                return index
            
    def fusion_clusters(self, iteration):
        """ 
        comparaison de distance entre tous les clusters
        Ca, Cb = plus petite distance entre 2 clusters
        Cab = centroid entre Ca et Cb
        retirer Ca et Cb de self.clusters
        ajouter Cab dans clusters
        """

        # compairson distance
        min_distance = None
        min_distance_cluster1 = None
        min_distance_cluster2 = None

        for i in range( len(self.clusters)-1 ):
            cluster1 = self.clusters[i]
            for j in range( (i+1), len(self.clusters) ):
                cluster2 = self.clusters[j]

                distance = self._euclidean_distance(cluster1["point"], cluster2["point"])

                if min_distance is None or min_distance > distance:
                    min_distance = distance
                    min_distance_cluster1 = cluster1
                    min_distance_cluster2 = cluster2

        if min_distance > 0.07:
            self.enough_cluster = True

        # formation du nouveau cluster
        total_x = 0
        total_y = 0

        points = []
        if len(min_distance_cluster1["clusters"]) == 0:
            points.append( min_distance_cluster1["point"] )
        else:
            points.extend( Util.get_all_points(min_distance_cluster1["clusters"]) )
        
        if len(min_distance_cluster2["clusters"]) == 0:
            points.append( min_distance_cluster2["point"] )
        else:
            points.extend( Util.get_all_points(min_distance_cluster2["clusters"]) )

        points = np.array(points)

        max_x = None
        max_y = None
       
        mean_x = np.unique(points[:,0]).sum() / np.unique(points[:,0]).shape[0]
        mean_y = np.unique(points[:,1]).sum() / np.unique(points[:,1]).shape[0]

        # new cluster
        id = self._new_id()
        point = [mean_x, mean_y]
        clusters = []
        clusters.append(min_distance_cluster1)
        clusters.append(min_distance_cluster2)
        files = []
        files.extend( min_distance_cluster1["files"] )
        files.extend( min_distance_cluster2["files"] )

        new_cluster = self._make_cluster( id, point, clusters, files)

        # Graph
        self.graph.add_point(point, id, iteration)
        self.graph.add_circle_from_cluster( new_cluster, iteration )
        # self.graph.add_legend()
        self.graph.save_graph()
        self.graph.remove_merged_cluster( new_cluster )

        # if new_cluster["id"] == 20:
        #     exit()
        # On enleve les anciens clusters
        index1 = self._get_index( min_distance_cluster1["id"] )
        index2 = self._get_index( min_distance_cluster2["id"] )

        del self.clusters[index1]
        del self.clusters[index2-1] # l'index a bouger suite a la premiere suppression


        # On ajoute le nouveau
        self.clusters.append( new_cluster )


    def _euclidean_distance(self,data_point_one, data_point_two):
        size = len(data_point_one)
        result = 0.0
        for i in range(size):
            f1 = float(data_point_one[i])   # feature for data one
            f2 = float(data_point_two[i])   # feature for data two
            tmp = f1 - f2
            result += pow(tmp, 2)
        result = math.sqrt(result)
        return result


    @staticmethod
    def _toString(clusters, lvl=0):
        string = ""
        for index, cluster in enumerate(clusters):
            padding = " " * (2 * (lvl+1))
            padding2 = " " * (2 * (lvl+2))
            padding3 = " " * (2 * (lvl+3))

            string += padding + "Cluster : {\n"
            string += padding2 + "id : " + str(cluster.id) + "\n"
            string += padding2 + "point : {\n"
            string += padding3 + "x : " + str(cluster.point[0]) + "\n"
            string += padding3 + "y : " + str(cluster.point[1]) + "\n"
            string += padding2 + "}\n"

            string += Clusters._toString(cluster.clusters, lvl+1)
            string += padding + "}\n"
        
        return string

    def _make_cluster(self, id, point, clusters, files):
        return {
            "id" : id,
            "point" : point,
            "clusters" : clusters,
            "files" : files
        }