import os

from image_util import ImageUtil
from data_extractor import DataExtractor

def getPersonneName(filename):
    arr = filename.split("_")
    name = arr[0] + "_" + arr[1]
    return name

folder = "clusters" # todo utiliser config

accuracy = 0
for directory in os.listdir(folder):
    names = {}
    for dirpath, dirnames, files in os.walk( os.path.join(folder, directory) ):

        for filename in files:
            name = getPersonneName( filename )

            if name not in names:
                names[name] = 1
            else:
                names[name] += 1

    if len(names) == 1:
        accuracy += 1
    else:
        maxi = None
        total = 0
        for element in names:
            total += names[element]
            
            if maxi is None or maxi < names[element]:
                maxi = names[element]

        accuracy += maxi / total

acc = accuracy / len(os.listdir(folder))
print("acc : ", acc)